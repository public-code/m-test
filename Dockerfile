FROM maven:3.6-jdk-8-alpine as builder

WORKDIR /app
COPY pom.xml .
RUN mvn -e -B dependency:resolve
COPY src ./src

RUN --mount=type=cache,target=/root/.m2 mvn -e -B package


FROM openjdk:8-jre-alpine

COPY --from=builder /app/target/*.jar app.jar

EXPOSE 8080

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom", "-jar", "/app.jar"]


