package com.musala.gateway.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import com.fasterxml.jackson.databind.JsonNode;
import com.musala.gateway.config.Helper;
import com.musala.gateway.entity.DeviceEntity.ConnectionStatus;
import com.musala.gateway.exception.DuplicationException;
import com.musala.gateway.exception.NotFoundException;
import com.musala.gateway.model.dto.Device;
import com.musala.gateway.model.dto.GatewayPool;

@DisplayName("testing device service")
@SpringBootTest
public class DeviceServiceTest {
  @Autowired
  private GatewayPoolService gatewayService;
  @Autowired
  private DeviceService deviceService;
  @Qualifier("gateway")
  @Autowired
  private JsonNode dataGateway;

  @Qualifier("devices")
  @Autowired
  private JsonNode dataDevices;

  //////////// HAPPY CASES (: ///////////////
  @Test
  @DisplayName("test create")
  public void testCreate() {
    JsonNode json = dataDevices.get("create").get(12);
    Device e = deviceService.create(Helper.readJson(json, Device.class));
    assertEquals(json.get("vendor").asText(), e.getVendor());
    assertEquals(json.get("uid").asLong(), e.getUid());
    assertNotNull(e.getCreatedDate());
    assertNotNull(e.getId());

  }

  @Test
  @DisplayName("test create and load by id")
  public void testCreateAndLoad() {
    JsonNode json = dataDevices.get("create").get(13);
    Device e1 = deviceService.create(Helper.readJson(json, Device.class));
    Device e = deviceService.retrieve(() -> e1.getId());

    assertEquals(json.get("vendor").asText(), e.getVendor());
    assertEquals(json.get("uid").asLong(), e.getUid());
    assertNotNull(e.getCreatedDate());
    assertNotNull(e.getId());
  }

  @Test
  @DisplayName("test Connection status before adding and after and after remove")
  public void testChangeToConnectStatus() {
    JsonNode jsonE = dataGateway.get("create").get(4);
    GatewayPool gatewayE = gatewayService.create(Helper.readJson(jsonE, GatewayPool.class));
    JsonNode json = dataDevices.get("create").get(14);
    Device e1 = deviceService.create(Helper.readJson(json, Device.class));
    Device e = deviceService.retrieve(() -> e1.getId());

    assertEquals(ConnectionStatus.DISCONNECTED, e.getStatus());


    gatewayService.addDevice(gatewayE.getId(), e1.getId());

    e = deviceService.retrieve(() -> e1.getId());

    assertEquals(ConnectionStatus.CONNECTED, e.getStatus());

    gatewayService.removeDevice(gatewayE.getId(), e1.getId());

    e = deviceService.retrieve(() -> e1.getId());

    assertEquals(ConnectionStatus.DISCONNECTED, e.getStatus());


  }
  //////////// Bad CASES ): ///////////////

  @Test
  @DisplayName("test notfound exception")
  public void testNotFoundException() {
    assertThrows(NotFoundException.class, () -> {
      deviceService.read(() -> Long.MAX_VALUE);
    });
  }

  @Test
  @DisplayName("test duplication exception")
  public void testDublicationException() {
    JsonNode json = dataDevices.get("create").get(15);
    Device entity = Helper.readJson(json, Device.class);
    deviceService.create(entity);
    assertThrows(DuplicationException.class, () -> {
      deviceService.create(entity);
    });
  }



}
