package com.musala.gateway.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import com.fasterxml.jackson.databind.JsonNode;
import com.musala.gateway.config.Helper;
import com.musala.gateway.exception.DuplicationException;
import com.musala.gateway.exception.NoMoreCapacityException;
import com.musala.gateway.exception.NotFoundException;
import com.musala.gateway.model.dto.Device;
import com.musala.gateway.model.dto.GatewayPool;

@DisplayName("testing gateway service")
@SpringBootTest
public class GatewayServiceTest {
  @Autowired
  private GatewayPoolService serviceGateway;
  @Autowired
  private DeviceService deviceService;
  @Qualifier("gateway")
  @Autowired
  private JsonNode dataGateway;

  @Qualifier("devices")
  @Autowired
  private JsonNode dataDevices;

  //////////// HAPPY CASES (: ///////////////
  @Test
  @DisplayName("test create")
  public void testCreate() {
    JsonNode json = dataGateway.get("create").get(0);
    GatewayPool e = serviceGateway.create(Helper.readJson(json, GatewayPool.class));
    assertEquals(json.get("ip").asText(), e.getIp());
    assertEquals(json.get("name").asText(), e.getName());
    assertEquals(json.get("serialNumber").asText(), e.getSerialNumber());
    assertNotNull(e.getCreatedDate());
    assertNotNull(e.getId());

  }

  @Test
  @DisplayName("test create and load by id")
  public void testCreateAndLoad() {
    JsonNode json = dataGateway.get("create").get(1);
    GatewayPool e1 = serviceGateway.create(Helper.readJson(json, GatewayPool.class));
    GatewayPool e = serviceGateway.retrieve(() -> e1.getId());

    assertEquals(json.get("ip").asText(), e.getIp());
    assertEquals(json.get("name").asText(), e.getName());
    assertEquals(json.get("serialNumber").asText(), e.getSerialNumber());
    assertNotNull(e.getCreatedDate());
    assertNotNull(e.getId());
  }

  //////////// Bad CASES ): ///////////////

  @Test
  @DisplayName("test notfound exception")
  public void testNotFoundException() {
    assertThrows(NotFoundException.class, () -> {
      serviceGateway.read(() -> Long.MAX_VALUE);
    });
  }

  @Test
  @DisplayName("test duplication exception")
  public void testDublicationException() {
    JsonNode json = dataGateway.get("create").get(2);
    GatewayPool entity = Helper.readJson(json, GatewayPool.class);
    serviceGateway.create(entity);
    assertThrows(DuplicationException.class, () -> {
      serviceGateway.create(entity);
    });
  }

  @Test
  @DisplayName("test no capacity exception")
  public void testNocapacityException() {
    JsonNode jsonDevice = dataDevices.get("create");
    JsonNode json = dataGateway.get("create").get(3);
    GatewayPool entity = Helper.readJson(json, GatewayPool.class);
    entity = serviceGateway.create(entity);

    for (int i = 0; i < 10; i++) {
      Device device = deviceService.create(Helper.readJson(jsonDevice.get(i), Device.class));
      serviceGateway.addDevice(entity.getId(), device.getId());
    }

    Device device = deviceService.create(Helper.readJson(jsonDevice.get(10), Device.class));


    long gateWayid = entity.getId();
    long deviceId = device.getId();
    assertThrows(NoMoreCapacityException.class, () -> {
      serviceGateway.addDevice(gateWayid, deviceId);
    });

  }

}
