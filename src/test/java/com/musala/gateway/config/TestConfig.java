package com.musala.gateway.config;

import java.io.IOException;
import java.io.InputStream;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import com.fasterxml.jackson.databind.JsonNode;

@Configuration
public class TestConfig {


  @Bean("gateway")
  public JsonNode jsonDataGateWay(@Value("classpath:gatwayTest.json") Resource res)
      throws IOException {

      try (InputStream inputStream = res.getInputStream();) {
        return Helper.readJson(inputStream);
      }

    }

    @Bean("devices")
    public JsonNode jsonDatadevices(@Value("classpath:devicesTest.json") Resource res)
        throws IOException {

      try (InputStream inputStream = res.getInputStream();) {
        return Helper.readJson(inputStream);
      }

    }
}
