package com.musala.gateway.config;

import java.io.IOException;
import java.io.InputStream;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Helper {

  private static final ObjectMapper mapper = new ObjectMapper();

  public static JsonNode readJson(InputStream inputStream) {
    try {
      return mapper.readTree(inputStream);
    } catch (IOException e) {
      return null;
    }
  }


  public static String readString(JsonNode node) {
    try {
      return mapper.writeValueAsString(node);
    } catch (JsonProcessingException e) {
      return "";
    }
  }


  public static <T> T readJson(JsonNode json, Class<T> clazz) {
    try {
      return mapper.treeToValue(json, clazz);
    } catch (IOException e) {
      return null;
    }
  }

}
