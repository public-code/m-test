package com.musala.gateway.integration;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.util.List;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.JsonNode;
import com.musala.gateway.config.AppConfig;
import com.musala.gateway.config.Helper;
import com.musala.gateway.config.TestConfig;
import com.musala.gateway.controller.GatewayPoolController;
import com.musala.gateway.exception.ControllerAdvisor;
import com.musala.gateway.exception.NotFoundException;
import com.musala.gateway.model.dto.GatewayPool;
import com.musala.gateway.repository.GatewayPoolRepo;
import com.musala.gateway.service.DeviceService;
import com.musala.gateway.service.GatewayPoolService;


@AutoConfigureMockMvc
@ContextConfiguration(
    classes = {GatewayPoolController.class, GatewayPoolService.class, ControllerAdvisor.class,
        GatewayPoolRepo.class,
        AppConfig.class, TestConfig.class})
@ExtendWith(SpringExtension.class)
@WebMvcTest
@DisplayName("testing gateway controller")
public class GatewayPoolControllerTest {
  @Autowired
  private MockMvc mockMvc;

  private HttpHeaders httpHeaders = new HttpHeaders();;

  @MockBean
  private DeviceService deviceService;

  @MockBean
  private GatewayPoolService gatewayService;

  @MockBean
  private GatewayPoolRepo repo;

  @Qualifier("gateway")
  @Autowired
  private JsonNode data;

  private final static String URI = "/v1/gateways";


  @Test
  @DisplayName("testing create new Gateway")
  public void testCreateController() throws Exception {

    when(gatewayService.create(any(GatewayPool.class)))
        .thenReturn(GatewayPool.builder().id(1L).build());

    mockMvc.perform(post(URI).headers(httpHeaders).contentType(MediaType.APPLICATION_JSON)
        .content(Helper.readString(data.get("create").get(0))))
        .andExpect(status().is2xxSuccessful())
        .andExpect(header().string("location", Matchers.endsWith(URI + "/1"))).andReturn();
  }

  @Test
  @DisplayName("testing invalid ip Gateway")
  public void testCreateInvalidDataController() throws Exception {

    mockMvc
        .perform(post(URI).headers(httpHeaders).contentType(MediaType.APPLICATION_JSON)
            .content(Helper.readString(data.get("invalidIp"))))
        .andExpect(status().is4xxClientError())
        .andExpect(content()
            .string(containsString("\"X-error-message\":\"The input have invalid IPv4 id \"")))
        .andExpect(content().string(containsString("\"X-error-code\":\"Validation\"")))
        .andReturn();
  }


  @Test
  @DisplayName("read Alldata Gateway")
  public void testreadAllDataController() throws Exception {
    @SuppressWarnings("unchecked")
    List<GatewayPool> d = Helper.readJson(data.get("readAll"), List.class);
    when(gatewayService.retrieves()).thenReturn(d);

    mockMvc.perform(get(URI).headers(httpHeaders).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is2xxSuccessful())
        .andExpect(content().string(Helper.readString(data.get("readAll"))))
        .andReturn();
  }

  @Test
  @DisplayName("read data Gateway")
  public void testreadDataController() throws Exception {
    GatewayPool d = Helper.readJson(data.get("create").get(0), GatewayPool.class);
    d.setId(1L);
    when(gatewayService.retrieve(any())).thenReturn(d);

    mockMvc
        .perform(get(URI + "/{id}", 1).headers(httpHeaders).contentType(MediaType.APPLICATION_JSON)
        )
        .andExpect(status().is2xxSuccessful())
        .andExpect(
            content().string(Helper.readString(data.get("returnGateWay"))))
        .andReturn();
  }

  @Test
  @DisplayName("read data NotfoundData Gateway")
  public void testreadNotfoundDataController() throws Exception {
    when(gatewayService.retrieve(any())).thenThrow(new NotFoundException("gateway"));
    mockMvc
        .perform(get(URI + "/{id}", 1).headers(httpHeaders).contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is4xxClientError())
        .andExpect(
            content().string(containsString("\"X-error-message\":\"gateway is Not found .\"")))
        .andExpect(content().string(containsString("\"X-error-code\":\"ent-nf01\"")))
        .andReturn();
  }

}
