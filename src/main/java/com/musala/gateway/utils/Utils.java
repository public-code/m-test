package com.musala.gateway.utils;

import org.apache.commons.validator.routines.InetAddressValidator;
import lombok.experimental.UtilityClass;

@UtilityClass
public class Utils {

	public static boolean isValidIPV4(String value) {
		if (value == null) {
			return false;
		}
        return InetAddressValidator.getInstance().isValidInet4Address(value);
	}
}
