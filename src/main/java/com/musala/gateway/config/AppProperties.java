package com.musala.gateway.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import lombok.Getter;
import lombok.Setter;

@ConfigurationProperties("gateway")
@Configuration
@Getter
@Setter
public class AppProperties {

  private int maxPool = 10;

}
