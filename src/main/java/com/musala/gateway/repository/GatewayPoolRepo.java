package com.musala.gateway.repository;

import org.springframework.stereotype.Repository;
import com.musala.gateway.entity.GatewaysPoolEntity;

@Repository
public interface GatewayPoolRepo extends GenericDAO<GatewaysPoolEntity> {

}
