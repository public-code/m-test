package com.musala.gateway.repository;

import org.springframework.stereotype.Repository;
import com.musala.gateway.entity.DeviceEntity;

@Repository
public interface DeviceRepo extends GenericDAO<DeviceEntity> {

}
