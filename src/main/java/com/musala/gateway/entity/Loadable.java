package com.musala.gateway.entity;

public interface Loadable<T> {
	T getId();
}
