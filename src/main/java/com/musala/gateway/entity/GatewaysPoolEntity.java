package com.musala.gateway.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity(name = "gatway_pool")
public class GatewaysPoolEntity extends AbstractAuditableEntity<Long> {
	@Id
	@Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gatway_pool_seq")
    @SequenceGenerator(name = "gatway_pool_seq", sequenceName = "gatway_pool_seq",
        allocationSize = 1)
	private Long id;
    @Column(length = 20, unique = true)
    private String serialNumber;
    @Column(length = 60)
    private String name;
    @Column(length = 15)
    private String ip;

	@Column
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "gateway", cascade = CascadeType.ALL)
	@Builder.Default
    Set<DeviceEntity> devices = new HashSet<>();


    public int countDevices() {
      return devices.size();
    }
}
