package com.musala.gateway.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.EqualsAndHashCode.Include;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity(name = "devices_entity")
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
public class DeviceEntity extends AbstractAuditableEntity<Long> {

	@Id
	@Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "devices_seq")
    @SequenceGenerator(name = "devices_seq", sequenceName = "devices_seq", allocationSize = 1)
	private Long id;

    @Column(unique = true, updatable = false)
    @Include
    private long uid;

    @Column(length = 15)
    private String vendor;

    @Column(updatable = true)
    @Builder.Default
    private ConnectionStatus status = ConnectionStatus.DISCONNECTED;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "gateway_id")
	@ToString.Exclude
    private GatewaysPoolEntity gateway;

    public enum ConnectionStatus {
      CONNECTED, DISCONNECTED
    }

}
