package com.musala.gateway.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.musala.gateway.entity.DeviceEntity.ConnectionStatus;
import com.musala.gateway.model.AbstractAuditableModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class Device extends AbstractAuditableModel<Long> {

  @ApiModelProperty(hidden = true, accessMode = AccessMode.READ_ONLY)
  @JsonProperty(index = 0, access = JsonProperty.Access.READ_ONLY)
  private Long id;

  @JsonProperty(index = 1)
  private long uid;
  @JsonProperty(index = 2)
  private String vendor;

  @ApiModelProperty(hidden = true, accessMode = AccessMode.READ_ONLY)
  @JsonProperty(index = 3, access = JsonProperty.Access.READ_ONLY)
  private ConnectionStatus status;

}
