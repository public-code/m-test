package com.musala.gateway.model.dto;

import java.util.Set;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.musala.gateway.model.AbstractAuditableModel;
import com.musala.gateway.validator.IPv4Validator;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiModelProperty.AccessMode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class GatewayPool extends AbstractAuditableModel<Long> {

  @ApiModelProperty(hidden = true, accessMode = AccessMode.READ_ONLY)
  @JsonProperty(index = 0, access = JsonProperty.Access.READ_ONLY)
  private Long id;

  @Size(min = 5, max = 20, message = "{lenght.pattern}")
  @JsonProperty(index = 1)
  private String serialNumber;
  @NotBlank(message = "name can't be null")
  @Size(min = 5, max = 60, message = "{lenght.pattern}")
  @JsonProperty(index = 2)
  private String name;
  @IPv4Validator
  @JsonProperty(index = 3)
  private String ip;


  @ApiModelProperty(hidden = true, accessMode = AccessMode.READ_ONLY)
  @JsonProperty(index = 4, access = JsonProperty.Access.READ_ONLY)
  Set<Device> devices;
}
