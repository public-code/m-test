package com.musala.gateway.model.mapper;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import com.musala.gateway.entity.DeviceEntity;
import com.musala.gateway.model.dto.Device;

@Component
@Qualifier("device_mapper")
public class DeviceMapper implements Mapper<DeviceEntity, Device> {

	@Override
	public DeviceEntity entityUpdate(DeviceEntity entity, Device dto) {
      entity.setVendor(dto.getVendor());
      return entity;
	}

	@Override
	public Device toDto(DeviceEntity entity) {
      return Device.builder().id(entity.getId()).status(entity.getStatus())
          .vendor(entity.getVendor()).uid(entity.getUid()).createdDate(entity.getCreatedDate())
          .lastModifiedDate(entity.getLastModifiedDate()).build();
	}

	@Override
	public DeviceEntity toEntity(Device dto) {
      return DeviceEntity.builder().vendor(dto.getVendor()).uid(dto.getUid()).build();
	}

}
