package com.musala.gateway.model.mapper;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import com.musala.gateway.entity.GatewaysPoolEntity;
import com.musala.gateway.model.dto.GatewayPool;

@Component
@Qualifier("gateway_pool")
public class GatewayPoolMapper implements Mapper<GatewaysPoolEntity, GatewayPool> {

	private DeviceMapper deviceMapper;

    public GatewayPoolMapper(DeviceMapper deviceMapper) {
      this.deviceMapper = deviceMapper;
	}

	@Override
	public GatewaysPoolEntity entityUpdate(GatewaysPoolEntity entity, GatewayPool dto) {
      entity.setName(dto.getName());
      entity.setSerialNumber(dto.getSerialNumber());
      entity.setIp(dto.getIp());
      return entity;
	}

	@Override
	public GatewayPool toDto(GatewaysPoolEntity entity) {
      return GatewayPool.builder().serialNumber(entity.getSerialNumber()).ip(entity.getIp())
          .name(entity.getName()).devices(deviceMapper.toDto(entity.getDevices()))
          .id(entity.getId()).createdDate(entity.getCreatedDate())
          .lastModifiedDate(entity.getLastModifiedDate()).build();

	}

	@Override
	public GatewaysPoolEntity toEntity(GatewayPool dto) {
      return GatewaysPoolEntity.builder().ip(dto.getIp()).name(dto.getName())
          .serialNumber(dto.getSerialNumber())
				.build();
	}

}
