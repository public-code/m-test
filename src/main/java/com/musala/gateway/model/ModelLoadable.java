package com.musala.gateway.model;

import java.io.Serializable;
import com.musala.gateway.entity.Loadable;

public interface ModelLoadable<T extends Serializable> extends Loadable<T> {
	void setId(T id);
}
