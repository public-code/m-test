package com.musala.gateway.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.musala.gateway.model.dto.Device;
import com.musala.gateway.service.DeviceService;

@RestController
@RequestMapping(DeviceController.PATH)
public class DeviceController extends AbstractCRUDController<Device> {
  public static final String PATH = "v1/devices";

	public DeviceController(DeviceService service) {
      super(service);
	}

    @Override
    public String controllerPath() {
      return PATH;
    }

}
