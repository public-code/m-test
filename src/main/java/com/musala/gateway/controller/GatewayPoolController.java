package com.musala.gateway.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.musala.gateway.model.dto.GatewayPool;
import com.musala.gateway.service.GatewayPoolService;

@RestController
@RequestMapping(GatewayPoolController.PATH)
public class GatewayPoolController extends AbstractCRUDController<GatewayPool> {
  public static final String PATH = "v1/gateways";

	public GatewayPoolController(GatewayPoolService service) {
		super(service);
	}

	@Override
	public String controllerPath() {
		return PATH;
	}

    @PostMapping("/{id}/device/{device_id}")
    public ResponseEntity<Void> saveAdders(@PathVariable("id") Long id,
        @PathVariable("device_id") long deviceId) {
      ((GatewayPoolService) service).addDevice(id, deviceId);
      return new ResponseEntity<>(HttpStatus.CREATED);
	}

    @DeleteMapping("/{id}/device/{device_id}")
    public ResponseEntity<Void> freeAddress(@PathVariable("id") Long id,
        @PathVariable("device_id") long deviceId) {

      ((GatewayPoolService) service).removeDevice(id, deviceId);

      return ResponseEntity.noContent().build();
	}
}
