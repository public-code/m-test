package com.musala.gateway.validator;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.LOCAL_VARIABLE;
import static java.lang.annotation.ElementType.PARAMETER;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = IpConstraintValidator.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({FIELD, PARAMETER, LOCAL_VARIABLE})
public @interface IPv4Validator {
  String message() default "{ipv4.validator}";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}
