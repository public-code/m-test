package com.musala.gateway.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import com.musala.gateway.utils.Utils;

public class IpConstraintValidator implements ConstraintValidator<IPv4Validator, String> {

  @Override
  public boolean isValid(String value, ConstraintValidatorContext context) {
    return Utils.isValidIPV4(value);
  }

}
