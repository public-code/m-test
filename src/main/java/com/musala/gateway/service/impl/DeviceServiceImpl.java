package com.musala.gateway.service.impl;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.musala.gateway.entity.DeviceEntity;
import com.musala.gateway.model.dto.Device;
import com.musala.gateway.model.mapper.Mapper;
import com.musala.gateway.repository.DeviceRepo;
import com.musala.gateway.service.AbstractServiceImpl;
import com.musala.gateway.service.DeviceService;

@Service
public class DeviceServiceImpl extends AbstractServiceImpl<DeviceEntity, Device, DeviceRepo>
		implements DeviceService {

	public DeviceServiceImpl(DeviceRepo repository,
        @Qualifier("device_mapper") Mapper<DeviceEntity, Device> mapper) {
		super(repository, mapper);
	}

	@Override
	public String name() {
      return "device";
	}


}
