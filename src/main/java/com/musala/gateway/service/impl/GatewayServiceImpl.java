package com.musala.gateway.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.musala.gateway.config.AppProperties;
import com.musala.gateway.entity.DeviceEntity.ConnectionStatus;
import com.musala.gateway.entity.GatewaysPoolEntity;
import com.musala.gateway.exception.NoMoreCapacityException;
import com.musala.gateway.model.dto.GatewayPool;
import com.musala.gateway.model.mapper.Mapper;
import com.musala.gateway.repository.GatewayPoolRepo;
import com.musala.gateway.service.AbstractServiceImpl;
import com.musala.gateway.service.DeviceService;
import com.musala.gateway.service.GatewayPoolService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class GatewayServiceImpl extends AbstractServiceImpl<GatewaysPoolEntity, GatewayPool, GatewayPoolRepo> implements GatewayPoolService {

  public GatewayServiceImpl(GatewayPoolRepo repository,
      @Qualifier("gateway_pool") Mapper<GatewaysPoolEntity, GatewayPool> mapper) {
		super(repository, mapper);
	}

	@Autowired
    private DeviceService deviceService;

    @Autowired
    private AppProperties properties;

    @Override
    public void addDevice(long id, long deviceid) {
      GatewaysPoolEntity entity = read(() -> id);

      if (entity.countDevices() == properties.getMaxPool()) {
        throw new NoMoreCapacityException();
      }

      deviceService.update(() -> deviceid, e -> {
        e.setStatus(ConnectionStatus.CONNECTED);
        e.setGateway(entity);
        entity.getDevices().add(e);
        create(entity);
      });
      log.info("add new device {},  in Gateway : {}", deviceid, id);
    }

    public void removeDevice(long id, long deviceid) {
      GatewaysPoolEntity entity = read(() -> id);

      deviceService.update(() -> deviceid, e -> {
        e.setStatus(ConnectionStatus.DISCONNECTED);
        e.setGateway(null);
        entity.getDevices().remove(e);
        create(entity);
      });
    }



    @Override
    public String name() {
      return "gateway";
    }



}
