package com.musala.gateway.service;

import com.musala.gateway.entity.GatewaysPoolEntity;
import com.musala.gateway.model.dto.GatewayPool;

public interface GatewayPoolService extends CRUD<GatewaysPoolEntity, GatewayPool> {

  public void addDevice(long id, long ipAddress);

  public void removeDevice(long id, long ipAddress);

}
