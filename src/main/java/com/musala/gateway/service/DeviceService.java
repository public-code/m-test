package com.musala.gateway.service;

import com.musala.gateway.entity.DeviceEntity;
import com.musala.gateway.model.dto.Device;

public interface DeviceService extends CRUD<DeviceEntity, Device> {

}
