package com.musala.gateway.service;

import java.util.List;
import java.util.function.Consumer;
import org.springframework.dao.DataIntegrityViolationException;
import com.musala.gateway.entity.Loadable;
import com.musala.gateway.exception.DuplicationException;
import com.musala.gateway.exception.NotFoundException;
import com.musala.gateway.model.ModelLoadable;
import com.musala.gateway.model.mapper.Mapper;
import com.musala.gateway.repository.GenericDAO;

public abstract class AbstractServiceImpl<E extends Loadable<Long>, D extends ModelLoadable<Long>, R extends GenericDAO<E>>
    implements CRUD<E, D> {

  protected R repository;
  protected Mapper<E, D> mapper;

  protected AbstractServiceImpl(R repository, Mapper<E, D> mapper) {
    this.repository = repository;
    this.mapper = mapper;
  }

  @Override
  public D create(D d) {
    return mapper.toDto(create(mapper.toEntity(d)));
  }

  @Override
  public D create(D d, Consumer<E> after) {
    E entity = create(mapper.toEntity(d));
    if (after != null) {
      after.accept(entity);
    }
    return mapper.toDto(entity);
  }

  @Override
  public E create(E e) {
    try {
      return repository.save(e);      
    }catch (DataIntegrityViolationException ex) {
      throw new DuplicationException(name());
    }
  }

  @Override
  public List<D> retrieves() {
    return mapper.toDto(repository.findAll());
  }

  @Override
  public D retrieve(Loadable<Long> id) {
    return mapper.toDto(read(id));
  }

  @Override
  public void update(D d) {
    E entity = read(d);
    mapper.entityUpdate(entity, d);
    repository.save(mapper.entityUpdate(entity, d));
  }

  @Override
  public E update(Loadable<Long> d, Consumer<E> beforeUpdate) {
    E entity = read(d);
    if (beforeUpdate != null) {
      beforeUpdate.accept(entity);
    }
    return repository.save(entity);
  }

  @Override
  public void delete(Loadable<Long> id) {
    delete(id, null);
  }

  @Override
  public void delete(Loadable<Long> id, Consumer<E> e) {
    E entity = read(id);
    if (e != null) {
      e.accept(entity);
    }
    repository.delete(entity);
  }

  @Override
  public E read(Loadable<Long> id) {
    if (id == null || id.getId() == null) {
      throw new NotFoundException(name());
    }
    return repository.findById(id.getId()).orElseThrow(() -> new NotFoundException(name()));
  }

  @SuppressWarnings("unchecked")
  @Override
  public R getRepository() {
    return repository;
  }

  @Override
  public Mapper<E, D> getMapper() {
    return mapper;
  }

  public abstract String name();
}
