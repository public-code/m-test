package com.musala.gateway.service;

import java.util.List;
import java.util.function.Consumer;
import org.springframework.transaction.annotation.Transactional;
import com.musala.gateway.entity.Loadable;
import com.musala.gateway.model.ModelLoadable;
import com.musala.gateway.model.mapper.Mapper;
import com.musala.gateway.repository.GenericDAO;

public interface CRUD<E extends Loadable<?>, D extends ModelLoadable<?>> {

  default void createAll(List<D> d) {
    d.forEach(this::create);
  }

  default void createAllEntity(List<E> d) {
    d.forEach(this::create);
  }

  D create(D d);

  D create(D d, Consumer<E> after);

  E create(E e);

  List<D> retrieves();

  D retrieve(Loadable<Long> id);

  void update(D d);

  E update(Loadable<Long> d, Consumer<E> beforeUpdate);

  @Transactional
  void delete(Loadable<Long> id);

  @Transactional
  void delete(Loadable<Long> id, Consumer<E> beforeDelete);

  E read(Loadable<Long> id);

  public <R extends GenericDAO<E>> R getRepository();

  public Mapper<E, D> getMapper();

}
